package com.greendict.sribarati.Service;

/**
 * Created by Rajkumar on 31-10-2016.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
 Created by Rajkumar
*/
public class Service {

//    private static final String BASE_URL = "http://192.168.1.12/Green/";

    private static final String BASE_URL = "http://greenship.sribarati.com/";

//    private static final String BASE_URL = "http://192.168.1.21:8080/";
//    http://192.168.0.75:8080/refimac_sales/service.php?opt=auth&user=emp001&password=213
//    private static final String BASE_URL1 = "http://192.168.1.9:8080/";

    private static final OkHttpClient httpClient = new OkHttpClient();

    static Gson gson = new GsonBuilder()
            .setLenient()
            .create();

    private static final Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());


    public static  <S> S createService(Class<S> serviceClass) {
        Retrofit retrofit = builder.client(httpClient).build();
        return retrofit.create(serviceClass);
    }


//    public static  <S> S createServiceHeader(Class<S> serviceClass) {
//
//        OkHttpClient okClient = new OkHttpClient.Builder()
//                .addInterceptor(
//                        new Interceptor() {
//                            @Override
//                            public Response intercept(Chain chain) throws IOException {
//                                Request original = chain.request();
//
//                                // Request customization: add request headers
//                                Request.Builder requestBuilder = original.newBuilder()
//                                        .header("UserName","e0001")
//                                        .method(original.method(), original.body());
//
//                                Request request = requestBuilder.build();
//                                return chain.proceed(request);
//                            }
//                        })
//                .build();
//
//        Retrofit retrofit = builder.client(okClient).build();
//        return retrofit.create(serviceClass);
//    }

}
