package com.greendict.sribarati.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.greendict.sribarati.DB.Main_DatabaseHandler;
import com.greendict.sribarati.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    String SearchDesc,SearachKey, Fav, status = null;

    private OnFragmentInteractionListener mListener;

    TextView TextViewSerachKey, TextViewSearchDesc, TextViewSerachKeyMain;

    ImageButton ShareButton, StarButton;

    Main_DatabaseHandler db;

    public DesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DesFragment newInstance(String param1, String param2) {
        DesFragment fragment = new DesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        SearachKey = getArguments().getString("serachkey");

        SearchDesc = getArguments().getString("searchdesc");

        Fav = getArguments().getString("fav");


         View v = inflater.inflate(R.layout.fragment_des, container, false);

         TextViewSerachKey = (TextView) v.findViewById(R.id.serachkey);
         TextViewSearchDesc = (TextView) v.findViewById(R.id.searchdesc);
//        TextViewSerachKeyMain = (TextView) v.findViewById(R.id.serachkeymain);
//
        TextViewSerachKey.setText(SearachKey);
//        TextViewSerachKeyMain.setText(SerachKey);
        TextViewSearchDesc.setText(SearchDesc);

        db = new Main_DatabaseHandler(getContext());

        ShareButton = (ImageButton) v.findViewById(R.id.ShareButton);
        ShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.sangamam.quotes&hl=en");
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareSub = "Sharing Using";
                String shareBody = SearachKey+ ": \n" +SearchDesc+ "\nShared via GreenDICT APP";

                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSub);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share using"));

            }
        });

//        String status = db.SelectTABLE_NAME(SearachKey)[3];

        StarButton = (ImageButton) v.findViewById(R.id.StarButton);

        if (Fav.equals("0")){
            StarButton.setImageResource(R.drawable.ic_star_white);
        }else if (Fav.equals("1")){
            StarButton.setImageResource(R.drawable.ic_star_yellow);
        }


        StarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//                String RowID = db.SelectTABLE_NAME(SearachKey)[0];

                try {
                    status = db.SelectTABLE_NAME(SearachKey)[3];
                }catch (Exception e){
                    status = "0";
                }


                if (status.equals("0") || status == null){
                    if (db.insertTABLE_NAME(SearachKey, SearchDesc) > 0){
                        StarButton.setImageResource(R.drawable.ic_star_yellow);
                        Toast.makeText(getContext(),"Added to favorite",Toast.LENGTH_SHORT).show();
                    }
                }else if (status.equals("1")){
                    if (db.deleteTitle(SearachKey) > 0){
                        StarButton.setImageResource(R.drawable.ic_star_white);
                        Toast.makeText(getContext(),"Favorite removed",Toast.LENGTH_SHORT).show();
                    }
                }

//                if (status.equals("0")){
//                    if (db.updateTABLE_NAME(RowID, "1") > 0){
//                        StarButton.setImageResource(R.drawable.ic_star_yellow);
//                        Toast.makeText(getContext(),"Added to favorite",Toast.LENGTH_SHORT).show();
//                    }
//                }else if (status.equals("1")){
//                    if (db.updateTABLE_NAME(RowID, "0") > 0){
//                        StarButton.setImageResource(R.drawable.ic_star_white);
//                        Toast.makeText(getContext(),"Favorite removed",Toast.LENGTH_SHORT).show();
//                    }
//                }



            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
