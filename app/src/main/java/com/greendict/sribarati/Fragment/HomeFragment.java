package com.greendict.sribarati.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.greendict.sribarati.API.Api;
import com.greendict.sribarati.DB.Main_DatabaseHandler;
import com.greendict.sribarati.Pojo.SearchKeyDataPojo;
import com.greendict.sribarati.Pojo.SearchKeyPojo;
import com.greendict.sribarati.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import retrofit2.Call;

import static com.greendict.sribarati.Service.Service.createService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements DesFragment.OnFragmentInteractionListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    // Listview Adapter
    ArrayAdapter<String> adapter, TestAdapter;

    // Search EditText
    AutoCompleteTextView inputSearch;

    int i = 0;

    String[] JsonStringValue[] = null, Key = null, Des = null, Testing = {"AA", "BB", "CC", "DD", "EE"};


    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;

//    PDFManager pdfManager;

    Main_DatabaseHandler db;

    Button btn_clear;

    private ProgressDialog progressBar;

    private int progressBarStatus = 0;

    String FavStatus;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View  v = inflater.inflate(R.layout.activity_main, container, false);

        db = new Main_DatabaseHandler(getContext());

        btn_clear = (Button) v.findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputSearch.setText(""); //clear edittext
                btn_clear.setVisibility(View.GONE);

                HomeFragment fragment = new HomeFragment();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame,fragment)
                        .addToBackStack(null)
                        .commit();
                fragmentManager.executePendingTransactions();
            }
        });

        inputSearch = (AutoCompleteTextView) v.findViewById(R.id.inputSearch);

//        ListView = (ListView) v.findViewById(R.id.list_view);


//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
//        if (!prefs.getBoolean("firstTime", false)) {
//            // <---- run your one time code here
//            try {
//                JSONObject obj = new JSONObject(loadJSONFromAsset());
//                JSONArray m_jArry = obj.getJSONArray("data");
//                JsonStringValue = new String[m_jArry.length()];
//                for (int i = 0; i < m_jArry.length(); i++) {
//                    JSONObject jo_inside = m_jArry.getJSONObject(i);
//                    db.insertTABLE_NAME(jo_inside.getString("serachkey"),jo_inside.getString("searchdesc"));
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            // mark first time has runned.
//            SharedPreferences.Editor editor = prefs.edit();
//            editor.putBoolean("firstTime", true);
//            editor.commit();
//        }





//        try {
//            JSONObject obj = new JSONObject(loadJSONFromAsset());
//            JSONArray m_jArry = obj.getJSONArray("data");
//            ArrayList<HashMap<String, String>> formList = new ArrayList<HashMap<String, String>>();
//            HashMap<String, String> m_li;
//            JsonStringValue = new String[m_jArry.length()];
//            for (int i = 0; i < m_jArry.length(); i++) {
//                JSONObject jo_inside = m_jArry.getJSONObject(i);
//                Log.d("Details-->", jo_inside.getString("serachkey"));
//                String serachkey = jo_inside.getString("serachkey");
//
//                JsonStringValue[i] = jo_inside.getString("serachkey");
//
//                String searchdesc = jo_inside.getString("searchdesc");
//
//                //Add your values in your `ArrayList` as below:
//                m_li = new HashMap<String, String>();
//                m_li.put("serachkey", serachkey);
//                m_li.put("searchdesc", searchdesc);
//
//                formList.add(m_li);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        progressBar = new ProgressDialog(getContext());
        progressBar.setCancelable(true);
        progressBar.setMessage("Loading ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        progressBarStatus = 0;

        Api apiService = createService(Api.class);

//        opt=KeyWordList&KeyWord=view
//        opt=KeyWordList&KeyWord=view

        Call<SearchKeyPojo> call = apiService.SEARCH_KEY_DATA_POJO_CALL("KeyWordList", "view");

        call.enqueue(new retrofit2.Callback<SearchKeyPojo>() {
            @Override
            public void onResponse(Call<SearchKeyPojo> call, retrofit2.Response<SearchKeyPojo> response) {
                progressBar.dismiss();
                int response_code = response.code();
                if (response_code == 200) {
                    String status = response.body().getSuccess();
                    if (status.equals("true")) {
                        SearchKeyDataPojo[] item = response.body().getData();
                        Key = new String[item.length];
                        Des = new String[item.length];
                        for (int i = 0; i<item.length; i++){
                            Key[i] = item[i].getKeyword();
//                            Des[i] = item[i].getKeyWordDesc();
                        }
                        // Adding items to listview
                        adapter = new ArrayAdapter<String>(getContext(), R.layout.list_item, R.id.serachkey, Key);
                        inputSearch.setThreshold(1);
                        inputSearch.setAdapter(adapter);
                    } else {

                        Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(getContext(), "Unable to connect to server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchKeyPojo> call, Throwable t) {
                progressBar.dismiss();
                try {
                    Toast.makeText(getContext(), "Server Problem, Please try again later", Toast.LENGTH_SHORT).show();

                } catch (NullPointerException e) {
                }
            }
        });

//        adapter = new ArrayAdapter<String>(getContext(), R.layout.list_item, R.id.serachkey, Testing);
//        inputSearch.setAdapter(adapter);

        inputSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selected = (String) parent.getItemAtPosition(position);

                try {
                    FavStatus = db.SelectTABLE_NAME(selected)[3];
                }catch (Exception e){
                    FavStatus = "0";
                }


                Api apiService = createService(Api.class);

//                    opt=ReqKeyword&KeyWord=Description&ReqKeyword=Accommodation%20Ladder

                Call<SearchKeyPojo> call = apiService.KEY_DATA_POJO_CALL("ReqKeyword", "Description", selected);

                call.enqueue(new retrofit2.Callback<SearchKeyPojo>() {
                    @Override
                    public void onResponse(Call<SearchKeyPojo> call, retrofit2.Response<SearchKeyPojo> response) {
                        int response_code = response.code();
                        if (response_code == 200) {
                            String status = response.body().getSuccess();
                            if (status.equals("true")) {
                                SearchKeyDataPojo[] item = response.body().getData();
                                Key = new String[item.length];
                                Des = new String[item.length];
                                for (int i = 0; i<item.length; i++){
                                    Key[i] = item[i].getKeyword();
                                    Des[i] = item[i].getDescription();

                                    Bundle bundleNew = new Bundle();
                                    bundleNew.putString("serachkey", Key[0]);
                                    bundleNew.putString("searchdesc", Des[0]);
                                    bundleNew.putString("fav",FavStatus);

                                    DesFragment fragment = new DesFragment();
                                    fragment.setArguments(bundleNew);
                                    FragmentManager fragmentManager = getFragmentManager();
                                    fragmentManager.beginTransaction()
                                            .replace(R.id.WordFrame,fragment)
                                            .addToBackStack(null)
                                            .commit();
                                    fragmentManager.executePendingTransactions();
                                }
                            } else {

                                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getContext(), "Unable to connect to server", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<SearchKeyPojo> call, Throwable t) {
                        try {
                            Toast.makeText(getContext(), "Server Problem, Please try again later", Toast.LENGTH_SHORT).show();

                        } catch (NullPointerException e) {
                        }
                    }
                });

            }
        });


        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!inputSearch.getText().toString().equals("")) { //if edittext include text
                    btn_clear.setVisibility(View.VISIBLE);
//                    inputSearch.setText(inputSearch.getText().toString());
                } else if (inputSearch.getText().toString().equals("")){ //not include text
                    btn_clear.setVisibility(View.GONE);
//                    inputSearch.setText("Edittext cleared!");
//                    Toast.makeText(EditTextActivity.this, "All texts have gone!!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("greenshipdec.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
