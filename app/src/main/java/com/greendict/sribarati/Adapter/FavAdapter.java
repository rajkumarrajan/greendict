package com.greendict.sribarati.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.greendict.sribarati.DB.Main_DatabaseHandler;
import com.greendict.sribarati.Pojo.SearchKeyDataPojo;
import com.greendict.sribarati.R;

import java.util.List;

/**
 * Created by Rajkumar Rajan on 13-02-2018.
 */

public class FavAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private List<SearchKeyDataPojo> mList;
    private String status;

    Main_DatabaseHandler db;

    public FavAdapter(Context context, List<SearchKeyDataPojo> mList){
        this.context = context;
        this.mList = mList;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView serachkey, searchdesc;
        ImageButton StarButton, ShareButton;
        LinearLayout FavCardLayout, errorHolder;
        private ViewHolder(View itemView) {
            super(itemView);
            serachkey = (TextView) itemView.findViewById(R.id.serachkey);
            searchdesc = (TextView) itemView.findViewById(R.id.searchdesc);
            StarButton = (ImageButton) itemView.findViewById(R.id.StarButton);
            ShareButton = (ImageButton) itemView.findViewById(R.id.ShareButton);
            FavCardLayout = (LinearLayout) itemView.findViewById(R.id.FavCardLayout);
            errorHolder = (LinearLayout) itemView.findViewById(R.id.errorHolder);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.custom_fav_card, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ViewHolder viewHolder = (ViewHolder) holder;
        final int pos = holder.getAdapterPosition();

        if (mList.size() > 0) {
            viewHolder.FavCardLayout.setVisibility(View.VISIBLE);
            viewHolder.errorHolder.setVisibility(View.GONE);
            viewHolder.serachkey.setText(mList.get(pos).getKeyword());
            viewHolder.searchdesc.setText(mList.get(pos).getDescription());
            if (mList.get(pos).getFav().equals("1")) {
                viewHolder.StarButton.setImageResource(R.drawable.ic_star_yellow);
            }

            ((ViewHolder) holder).ShareButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareSub = "Sharing Using";
                    String shareBody = mList.get(pos).getKeyword() + ": \n" + mList.get(pos).getDescription() + "\nShared via GreenDICT APP";

                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, shareSub);
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    context.startActivity(Intent.createChooser(sharingIntent, "Share using"));
                }
            });

            ((ViewHolder) holder).StarButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    db = new Main_DatabaseHandler(context);

                    try {
                        status = db.SelectTABLE_NAME(mList.get(pos).getKeyword())[3];
                    } catch (Exception e) {
                        status = "0";
                    }


                    if (status.equals("0") || status == null) {
                        if (db.insertTABLE_NAME(mList.get(pos).getKeyword(), mList.get(pos).getDescription()) > 0) {
                            viewHolder.StarButton.setImageResource(R.drawable.ic_star_yellow);
                            Toast.makeText(context, "Added to favorite", Toast.LENGTH_SHORT).show();
                        }
                    } else if (status.equals("1")) {
                        if (db.deleteTitle(mList.get(pos).getKeyword()) > 0) {
                            viewHolder.StarButton.setImageResource(R.drawable.ic_star_white);
                            Toast.makeText(context, "Favorite removed", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

        }else {
            viewHolder.FavCardLayout.setVisibility(View.GONE);
            viewHolder.errorHolder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
