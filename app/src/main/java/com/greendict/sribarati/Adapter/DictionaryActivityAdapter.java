package com.greendict.sribarati.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.greendict.sribarati.Pojo.SearchKeyDataPojo;
import com.greendict.sribarati.R;

import java.util.ArrayList;

/**
 * Created by Rajkumar on 12-03-2018.
 */

public class DictionaryActivityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private ArrayList<SearchKeyDataPojo> mList;
    private DictSelectedData dictSelectedData;

    public interface DictSelectedData {

        void DictData(String Keyword, String Description);

    }

    public DictionaryActivityAdapter(Context context, ArrayList<SearchKeyDataPojo> mList, DictSelectedData dictSelectedData) {
        this.context = context;
        this.mList = mList;
        this.dictSelectedData = dictSelectedData;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView KeyWord;
        Button button;
        private ViewHolder(View itemView) {
            super(itemView);
            KeyWord = (TextView) itemView.findViewById(R.id.KeyWord);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View menuItemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.dictionary_activity_adapter_card, parent, false);
        viewHolder = new ViewHolder(menuItemLayoutView);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.KeyWord.setText(mList.get(position).getKeyword());
        viewHolder.KeyWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dictSelectedData.DictData(mList.get(position).getKeyword(), mList.get(position).getDescription());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
