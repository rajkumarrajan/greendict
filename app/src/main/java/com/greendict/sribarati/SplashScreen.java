package com.greendict.sribarati;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.greendict.sribarati.Activity.NavigationActivity;
import com.greendict.sribarati.Activity.SignInActivity;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);


        int SPLASH_TIME_OUT = 3000;
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                SharedPreferences sharedPreferences = PreferenceManager
                        .getDefaultSharedPreferences(SplashScreen.this);

                if (sharedPreferences.getBoolean("LoginStatus", Boolean.parseBoolean(null))){
                    Intent i = new Intent(SplashScreen.this, NavigationActivity.class);
                    startActivity(i);

                    // close this activity
                    finish();

                }else {
                    Intent i = new Intent(SplashScreen.this, SignInActivity.class);
                    startActivity(i);

                    // close this activity
                    finish();
                }



            }
        }, SPLASH_TIME_OUT);
    }

}
