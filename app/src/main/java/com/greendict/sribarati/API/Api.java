package com.greendict.sribarati.API;

import com.greendict.sribarati.Pojo.SearchKeyDataPojo;
import com.greendict.sribarati.Pojo.SearchKeyPojo;
import com.greendict.sribarati.Pojo.SignInPojo;
import com.greendict.sribarati.Pojo.SignUpPojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Rajkumar on 09-01-2017.
 */

public interface Api {

    //    opt=KeyWordList&KeyWord=view
    @GET("RestApi.php")
    Call<SearchKeyPojo> SEARCH_KEY_DATA_POJO_CALL(@Query("opt") String opt, @Query("KeyWord") String KeyWord);

    //    opt=ReqKeyword&KeyWord=Description&ReqKeyword=Accommodation%20Ladder
    @GET("RestApi.php")
    Call<SearchKeyPojo> KEY_DATA_POJO_CALL(@Query("opt") String ReqKeyword, @Query("KeyWord") String Description,
                                       @Query("ReqKeyword") String Accommodation);

    //    opt=auth&request=signup&UserName=UserName&MobileNo=MobileNo&EmailId=EmailId&Password=Password&ConfirmPass=ConfirmPass
    @GET("RestApi.php")
    Call<SignUpPojo> SIGN_UP_POJO_CALL(@Query("opt") String auth, @Query("request") String signup,
                                       @Query("UserName") String UserName,
                                       @Query("MobileNo") String MobileNo,
                                       @Query("EmailId") String EmailId ,
                                        @Query("Password") String Password,
                                       @Query("ConfirmPass") String ConfirmPass);

    //    opt=auth&request=signin&Id=MobileNo&Password=Password
    @GET("RestApi.php")
    Call<SignInPojo> SIGN_IN_POJO_CALL(@Query("opt") String auth, @Query("request") String signin,
                                       @Query("Id") String MobileNo,
                                       @Query("Password") String Password);


}
