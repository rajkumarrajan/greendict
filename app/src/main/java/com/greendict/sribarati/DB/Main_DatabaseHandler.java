package com.greendict.sribarati.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.greendict.sribarati.Pojo.SearchKeyDataPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajkumar Rajan on 12-02-2018.
 */

public class Main_DatabaseHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "GreenShip";

    // Labels table name
    private static final String TABLE_NAME = "Table1";

    // Labels Table Columns names
    private static final String ID = "ID";
    private static final String SearachKey= "searachkey";
    private static final String SearchDesc = "searchdesc";
    private static final String FavField = "fav";

    public Main_DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String Table = "CREATE TABLE " + TABLE_NAME + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SearachKey + " TEXT," + SearchDesc + " TEXT," + FavField + " TEXT)";
        sqLiteDatabase.execSQL(Table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);

        // Create tables again
        onCreate(sqLiteDatabase);

    }

    /**
     * Inserting new lable into lables table
     * */
    public long insertTABLE_NAME(String serachkey, String searchdesc){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SearachKey, serachkey);
        values.put(SearchDesc, searchdesc);
        values.put(FavField, "1");

        // Inserting Row
        long id = db.insert(TABLE_NAME, null, values);

        db.close(); // Closing database connection
        return id;
    }

    public String[] SelectTABLE_NAME(String serachkey){

        String serachKey[] = null;
        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE searachkey = '"+ serachkey +"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                serachKey = new String[4];
                serachKey[0] = cursor.getString(0);
                serachKey[1] = cursor.getString(1);
                serachKey[2] = cursor.getString(2);
                serachKey[3] = cursor.getString(3);
            } while (cursor.moveToNext());
        }
        // closing connection
        cursor.close();
        db.close();
        return serachKey;
    }

    public List<SearchKeyDataPojo> SelectAllDataTABLE_NAME(){

        List<SearchKeyDataPojo> data = new ArrayList<SearchKeyDataPojo>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                SearchKeyDataPojo D = new SearchKeyDataPojo();
                D.setId(cursor.getString(0));
                D.setKeyword(cursor.getString(1));
                D.setDescription(cursor.getString(2));
                D.setFav(cursor.getString(3));
                data.add(D);
            } while (cursor.moveToNext());
        }
        // closing connection
        cursor.close();
        db.close();
        return data;
    }

    /*
 * Updating a Table
 */
    public int updateTABLE_NAME(String RowID, String Value) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FavField, Value);

        // updating row
        return db.update(TABLE_NAME, values, ID + " = "+ RowID,null);
    }

    //---deletes a particular title---
    public int deleteTitle(String searachKEY)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, SearachKey + " = '" + searachKEY +"'", null);
    }

    public void delTable(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS Table1");
        final String Table = "CREATE TABLE " + TABLE_NAME + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + SearachKey + " TEXT," + SearchDesc + " TEXT," + FavField + " TEXT)";
        db.execSQL(Table);
    }
}
