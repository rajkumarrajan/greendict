package com.greendict.sribarati.Pojo;

/**
 * Created by Rajkumar Rajan on 02-03-2018.
 */

public class SearchKeyDataPojo {
    private String description;

    private String keyword;

    private String Id;

    private String Fav;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFav() {
        return Fav;
    }

    public void setFav(String fav) {
        Fav = fav;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getKeyword ()
    {
        return keyword;
    }

    public void setKeyword (String keyword)
    {
        this.keyword = keyword;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [description = "+description+", keyword = "+keyword+"]";
    }
}
