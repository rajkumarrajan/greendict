package com.greendict.sribarati.Pojo;

/**
 * Created by Rajkumar Rajan on 02-03-2018.
 */

public class SearchKeyPojo {

    private SearchKeyDataPojo[] data;

    private String success;

    public SearchKeyDataPojo[] getData ()
    {
        return data;
    }

    public void setData (SearchKeyDataPojo[] data)
    {
        this.data = data;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [data = "+data+", success = "+success+"]";
    }
}
