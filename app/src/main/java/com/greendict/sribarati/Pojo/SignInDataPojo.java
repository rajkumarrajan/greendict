package com.greendict.sribarati.Pojo;

/**
 * Created by Rajkumar on 12-03-2018.
 */

public class SignInDataPojo {
    private String Password;

    private String UserName;

    private String EmailId;

    private String MobileNo;

    public String getPassword ()
    {
        return Password;
    }

    public void setPassword (String Password)
    {
        this.Password = Password;
    }

    public String getUserName ()
    {
        return UserName;
    }

    public void setUserName (String UserName)
    {
        this.UserName = UserName;
    }

    public String getEmailId ()
    {
        return EmailId;
    }

    public void setEmailId (String EmailId)
    {
        this.EmailId = EmailId;
    }

    public String getMobileNo ()
    {
        return MobileNo;
    }

    public void setMobileNo (String MobileNo)
    {
        this.MobileNo = MobileNo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Password = "+Password+", UserName = "+UserName+", EmailId = "+EmailId+", MobileNo = "+MobileNo+"]";
    }
}
