package com.greendict.sribarati.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.greendict.sribarati.Adapter.FavAdapter;
import com.greendict.sribarati.DB.Main_DatabaseHandler;
import com.greendict.sribarati.Pojo.SearchKeyDataPojo;
import com.greendict.sribarati.R;

import java.util.ArrayList;
import java.util.List;

public class FavouritesActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    Main_DatabaseHandler db;

    List<SearchKeyDataPojo> mList = new ArrayList<>();

    FavAdapter favAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("My Favourites");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);

        db = new Main_DatabaseHandler(this);

        List<SearchKeyDataPojo> tableDataPojos = db.SelectAllDataTABLE_NAME();

        for (int i = 0; i < tableDataPojos.size(); i++){
            if (tableDataPojos.get(i).getFav().equals("1")){
                String id = tableDataPojos.get(i).getId();
                String serachkey = tableDataPojos.get(i).getKeyword();
                String searchdesc = tableDataPojos.get(i).getDescription();
                String fav = tableDataPojos.get(i).getFav();

                SearchKeyDataPojo tableDataPojo = new SearchKeyDataPojo();
                tableDataPojo.setId(id);
                tableDataPojo.setKeyword(serachkey);
                tableDataPojo.setDescription(searchdesc);
                tableDataPojo.setFav(fav);

                mList.add(tableDataPojo);
                favAdapter = new FavAdapter(this,mList);
                recyclerView.setAdapter(favAdapter);
            }
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
