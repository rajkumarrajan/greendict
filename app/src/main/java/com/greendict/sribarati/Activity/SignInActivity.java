package com.greendict.sribarati.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.greendict.sribarati.API.Api;
import com.greendict.sribarati.Pojo.SignInDataPojo;
import com.greendict.sribarati.Pojo.SignInPojo;
import com.greendict.sribarati.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

import static com.greendict.sribarati.Service.Service.createService;

public class SignInActivity extends AppCompatActivity {

    Button Login,signup;

    EditText editID,editPassword;

    private ProgressDialog progressBar;
    private int progressBarStatus = 0;

    List<SignInDataPojo> signInDataPojos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editID = (EditText) findViewById(R.id.ID);
        editPassword = (EditText) findViewById(R.id.Password);
        Login = (Button) findViewById(R.id.Login);

        signup = (Button) findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(i);
            }
        });

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!CheckEmpty(editID) && !CheckEmpty(editPassword)) {
                    progressBar = new ProgressDialog(SignInActivity.this);
                    progressBar.setCancelable(true);
                    progressBar.setMessage("Loading ...");
                    progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressBar.setProgress(0);
                    progressBar.setMax(100);
                    progressBar.show();
                    progressBarStatus = 0;

//                    opt=auth&request=signin&Id=MobileNo&Password=Password

                    Api apiService = createService(Api.class);

                    Call<SignInPojo> call = apiService.SIGN_IN_POJO_CALL("auth", "signin",
                            editID.getText().toString(), editPassword.getText().toString());

                    call.enqueue(new retrofit2.Callback<SignInPojo>() {
                        @Override
                        public void onResponse(Call<SignInPojo> call, retrofit2.Response<SignInPojo> response) {
                            int response_code = response.code();
                            if (response_code == 200) {
                                String status = response.body().getSuccess();
                                if (status.equals("true")) {

                                    signInDataPojos = new ArrayList<>();
                                    SignInDataPojo[] item = response.body().getData();

                                    for (SignInDataPojo anItem : item) {
                                        String UserName = anItem.getUserName();
                                        String Password = anItem.getPassword();
                                        String MobileNo = anItem.getMobileNo();
                                        String EmailID = anItem.getEmailId();
                                    }
                                    finish();

                                    SharedPreferences sharedPreferences = PreferenceManager
                                            .getDefaultSharedPreferences(SignInActivity.this);
                                    SharedPreferences.Editor editor = sharedPreferences.edit();
                                    editor.putBoolean("LoginStatus", true);
                                    editor.apply();

                                    Intent i = new Intent(SignInActivity.this, NavigationActivity.class);
                                    startActivity(i);
                                    progressBar.dismiss();
                                } else {

                                    Toast.makeText(SignInActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    progressBar.dismiss();
                                }

                            } else {
                                Toast.makeText(SignInActivity.this, "Unable to connect to server", Toast.LENGTH_SHORT).show();
                                progressBar.dismiss();
                            }
                            progressBar.dismiss();
                        }

                        @Override
                        public void onFailure(Call<SignInPojo> call, Throwable t) {
                            try {
                                Toast.makeText(SignInActivity.this, "Server Problem, Please try again later", Toast.LENGTH_SHORT).show();

                            } catch (NullPointerException e) {
                            }
                            progressBar.dismiss();
                        }
                    });

                }else {
                    Toast.makeText(SignInActivity.this, "Fill all the fields", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public boolean CheckEmpty(EditText editText){
        return editText.getText().toString().isEmpty();
    }

}
