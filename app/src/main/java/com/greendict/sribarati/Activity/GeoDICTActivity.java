package com.greendict.sribarati.Activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.greendict.sribarati.R;

public class GeoDICTActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geo_dict);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("GreenDICT");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        text = (TextView) findViewById(R.id.text);
        text.setText("GeoDICT is a handy reference of commonly used terms and acronyms for people " +
                "involved in Ship Design, Construction, Survey, Maritime, Shipping, Chartering, " +
                "Yachting, Sailing, Nautical, Marine Engineering, Shipbuilding, Information and " +
                "Communications Technologies and related areas. It helps mariners, captain, naval " +
                "architect, marine engineers, freight forwarders, shipping agents, customs brokers, " +
                "logistics service providers, stevedoring companies, CFS operators, exporters, " +
                "importers, chandlers, warehousing companies, ship building and repair firms.");

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
