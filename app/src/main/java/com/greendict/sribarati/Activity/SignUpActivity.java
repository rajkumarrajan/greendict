package com.greendict.sribarati.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.greendict.sribarati.API.Api;
import com.greendict.sribarati.Pojo.SignUpPojo;
import com.greendict.sribarati.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Arrays;

import retrofit2.Call;

import static com.greendict.sribarati.Service.Service.createService;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{

    //google login api and instance
    private GoogleApiClient googleApiClient;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;

    //this is for facebook signin
    LoginButton loginButton;
    CallbackManager callbackManager;

    TextView name1,email1;

    //this is for google sign in
    Button Submit,signingoogle;
    LinearLayout Proof;

    private ProgressDialog progressBar;
    private int progressBarStatus = 0;

    EditText Name, Mobile, Email, Password, Re_Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        callbackManager = CallbackManager.Factory.create();

        name1 = (TextView)findViewById(R.id.Name1);
        email1 = (TextView)findViewById(R.id.Email1);

        signingoogle = (Button) findViewById(R.id.signingoogle);
        signingoogle.setOnClickListener(this);

        Proof = (LinearLayout)findViewById(R.id.Proof);

        //1.google sign in option for creating gso

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //2.google apo client create
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        //facebook fuctionality
        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday", "user_friends"));

        callbackManager = CallbackManager.Factory.create();
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Log.d("mylog", "Successful: " + loginResult.getAccessToken());

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Application code

                        try {
                            String name = object.getString("name");
                            String email = object.getString("email");
                            String birthday = object.getString("birthday"); // 01/31/1980 format

                            Toast.makeText(SignUpActivity.this, "Name: "+ name + "Email: "+ email, Toast.LENGTH_LONG).show();

                            SocialMediaSignUp(name, email);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.d("mylog", "Successful: " + "onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d("mylog", "Successful: " + exception.toString());
            }
        });

        Name = (EditText) findViewById(R.id.Name);
        Mobile = (EditText) findViewById(R.id.Phone);
        Email = (EditText) findViewById(R.id.Email);
        Password = (EditText) findViewById(R.id.Password);
        Re_Password = (EditText) findViewById(R.id.Co_Password);

        Submit = (Button) findViewById(R.id.Submit);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                progressBar = new ProgressDialog(SignUpActivity.this);
                progressBar.setCancelable(true);
                progressBar.setMessage("Loading ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.setProgress(0);
                progressBar.setMax(100);
                progressBar.show();
                progressBarStatus = 0;

                if (!CheckEmpty(Name) && !CheckEmpty(Mobile) && !CheckEmpty(Email)
                        && !CheckEmpty(Password) && !CheckEmpty(Re_Password)) {
                    if (Password.getText().toString().equals(Re_Password.getText().toString()) ) {
                        if (isValidEmail(Email.getText().toString())) {

                            //                opt=auth&request=signup&UserName=UserName&MobileNo=MobileNo&EmailId=EmailId&Password=Password&ConfirmPass=ConfirmPass

                            Api apiService = createService(Api.class);

                            Call<SignUpPojo> call = apiService.SIGN_UP_POJO_CALL("auth","signup", Name.getText().toString(), Mobile.getText().toString(),
                                    Email.getText().toString(), Password.getText().toString(), Re_Password.getText().toString());

                            call.enqueue(new retrofit2.Callback<SignUpPojo>() {
                                @Override
                                public void onResponse(Call<SignUpPojo> call, retrofit2.Response<SignUpPojo> response) {
                                    int response_code = response.code();
                                    if (response_code == 200) {
                                        String status = response.body().getSuccess();
                                        if (status.equals("true")) {
                                            finish();
                                            Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                            Intent i = new Intent(SignUpActivity.this, SignInActivity.class);
                                            startActivity(i);
                                            progressBar.dismiss();
                                        } else {

                                            Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                            progressBar.dismiss();
                                        }

                                    } else {
                                        Toast.makeText(SignUpActivity.this, "Unable to connect to server", Toast.LENGTH_SHORT).show();
                                        progressBar.dismiss();
                                    }
                                    progressBar.dismiss();
                                }

                                @Override
                                public void onFailure(Call<SignUpPojo> call, Throwable t) {
                                    try {
                                        Toast.makeText(SignUpActivity.this, "Server Problem, Please try again later", Toast.LENGTH_SHORT).show();

                                    } catch (NullPointerException e) {
                                    }
                                    progressBar.dismiss();
                                }
                            });

                        }else {
                            Toast.makeText(SignUpActivity.this, "Enter valid E-Mail ID", Toast.LENGTH_SHORT).show();
                            progressBar.dismiss();
                        }
                    }else {
                        Toast.makeText(SignUpActivity.this, "Both password are not matching", Toast.LENGTH_SHORT).show();
                        progressBar.dismiss();
                    }
                }else {
                    Toast.makeText(SignUpActivity.this, "Enter Value for all the fields", Toast.LENGTH_SHORT).show();
                    progressBar.dismiss();
                }


            }
        });
    }

    private void SocialMediaSignUp(String name, String email) {

        progressBar = new ProgressDialog(SignUpActivity.this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Loading ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        progressBarStatus = 0;

//        opt=auth&request=signup&UserName=UserName&MobileNo=MobileNo&EmailId=EmailId&Password=Password&ConfirmPass=ConfirmPass

        Api apiService = createService(Api.class);

        Call<SignUpPojo> call = apiService.SIGN_UP_POJO_CALL("auth","signup", name, "",
                email, "", "");

        call.enqueue(new retrofit2.Callback<SignUpPojo>() {
            @Override
            public void onResponse(Call<SignUpPojo> call, retrofit2.Response<SignUpPojo> response) {
                int response_code = response.code();
                if (response_code == 200) {
                    String status = response.body().getSuccess();
                    if (status.equals("true")) {
                        finish();

                        SharedPreferences sharedPreferences = PreferenceManager
                                .getDefaultSharedPreferences(SignUpActivity.this);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putBoolean("LoginStatus", true);
                        editor.apply();

                        Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(SignUpActivity.this, NavigationActivity.class);
                        startActivity(i);
                        progressBar.dismiss();
                    } else {

                        Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.dismiss();
                    }

                } else {
                    Toast.makeText(SignUpActivity.this, "Unable to connect to server", Toast.LENGTH_SHORT).show();
                    progressBar.dismiss();
                }
                progressBar.dismiss();
            }

            @Override
            public void onFailure(Call<SignUpPojo> call, Throwable t) {
                try {
                    Toast.makeText(SignUpActivity.this, "Server Problem, Please try again later", Toast.LENGTH_SHORT).show();

                } catch (NullPointerException e) {
                }
                progressBar.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.signingoogle:
                signin();
                break;
            case R.id.fb:
                loginButton.performClick();
//                fblogin();
                break;

            case R.id.Logout:
                signout();
                break;
        }
    }

    //7.this will be get the data from google account and result set in to our user interface
    private void handlerResult(GoogleSignInResult result){
        if (result.isSuccess()) {

            GoogleSignInAccount acct = result.getSignInAccount();

            String Name = acct.getDisplayName();
            String Email = acct.getEmail();

            Log.e(TAG, "Name: " + Name + ", email: " + Email);

//            Toast.makeText(SignUpActivity.this, "Name: "+ personName + "Email: "+ Email, Toast.LENGTH_LONG).show();

            SocialMediaSignUp(Name, Email);

            name1.setText(Name);
            email1.setText(Email);
            Toast.makeText(this,"Success", Toast.LENGTH_SHORT).show();

//            updataUI(true);
        }

    }


    private void signin() {

        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signout() {

        Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
//                        updataUI(false);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handlerResult(result);

        }
    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handlerResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    handlerResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    public boolean CheckEmpty(EditText editText){
        return editText.getText().toString().isEmpty();
    }

    public final static boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}
