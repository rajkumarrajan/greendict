package com.greendict.sribarati.Activity;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;


import com.greendict.sribarati.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends Fragment {

    // List view
    private ListView ListView;

    // Listview Adapter
    ArrayAdapter<String> adapter, TestAdapter;

    // Search EditText
    AutoCompleteTextView inputSearch;

    int i = 0;

    String JsonStringValue[] = null;


    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;

//    PDFManager pdfManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        // Inflate the layout for this fragment
        View  v = inflater.inflate(R.layout.activity_main, container, false);

//        ListView = (ListView) v.findViewById(R.id.list_view);
        inputSearch = (AutoCompleteTextView ) v.findViewById(R.id.inputSearch);

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("data");
            ArrayList<HashMap<String, String>> formList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> m_li;
            JsonStringValue = new String[m_jArry.length()];
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Log.d("Details-->", jo_inside.getString("serachkey"));
                String serachkey = jo_inside.getString("serachkey");

                JsonStringValue[i] = jo_inside.getString("serachkey");

                String searchdesc = jo_inside.getString("searchdesc");

                //Add your values in your `ArrayList` as below:
                m_li = new HashMap<String, String>();
                m_li.put("serachkey", serachkey);
                m_li.put("searchdesc", searchdesc);

                formList.add(m_li);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Adding items to listview
        adapter = new ArrayAdapter<String>(getContext(), R.layout.list_item, R.id.serachkey, JsonStringValue);
//        ListView.setAdapter(MainActivity.this.adapter);

        inputSearch.setAdapter(adapter);

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                MainActivity.this.adapter.getFilter().filter(cs);


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

                if (arg0 == null){
                    ListView.setVisibility(View.GONE);
                }

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                if (arg0.toString() == null)
                {
                    ListView.setVisibility(View.GONE);
                }
            }
        });

//        ListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//
////                JsonStringValue[i].;
//                Toast.makeText(getContext(),
//                        ((TextView) view).getText(), Toast.LENGTH_SHORT).show();
////                String[] TestString = null;
////
////                TestString[i++] = String.valueOf(((TextView) view).getText());
////                TestAdapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.product_name, TestString);
//            }
//        });
        return v;
    }


    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("greenshipdec.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
