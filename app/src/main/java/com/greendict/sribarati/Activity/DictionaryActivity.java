package com.greendict.sribarati.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.greendict.sribarati.API.Api;
import com.greendict.sribarati.Adapter.DictionaryActivityAdapter;
import com.greendict.sribarati.Fragment.DictionaryDetailFragment;
import com.greendict.sribarati.Pojo.SearchKeyDataPojo;
import com.greendict.sribarati.Pojo.SearchKeyPojo;
import com.greendict.sribarati.R;

import java.util.ArrayList;

import retrofit2.Call;

import static com.greendict.sribarati.Service.Service.createService;

public class DictionaryActivity extends AppCompatActivity implements DictionaryDetailFragment.OnFragmentInteractionListener{

    RecyclerView recyclerView;


    private ProgressDialog progressBar;
    private int progressBarStatus = 0;
    private ArrayList<SearchKeyDataPojo> mList;

    private FrameLayout frame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Dictionary");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        LinearLayoutManager llm = new LinearLayoutManager(DictionaryActivity.this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);
        recyclerView.setVisibility(View.VISIBLE);

        frame = (FrameLayout) findViewById(R.id.frame);
        frame.setVisibility(View.GONE);

        progressBar = new ProgressDialog(DictionaryActivity.this);
        progressBar.setCancelable(true);
        progressBar.setMessage("Loading ...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setProgress(0);
        progressBar.setMax(100);
        progressBar.show();
        progressBarStatus = 0;

        //        opt=KeyWordList&KeyWord=view
        Api apiService = createService(Api.class);

        Call<SearchKeyPojo> call = apiService.SEARCH_KEY_DATA_POJO_CALL("KeyWordList", "view");

        call.enqueue(new retrofit2.Callback<SearchKeyPojo>() {
            @Override
            public void onResponse(Call<SearchKeyPojo> call, retrofit2.Response<SearchKeyPojo> response) {
                progressBar.dismiss();
                int response_code = response.code();
                if (response_code == 200) {
                    String status = response.body().getSuccess();
                    if (status.equals("true")) {
                        SearchKeyDataPojo[] item = response.body().getData();

                        mList = new ArrayList<>();

                        for (SearchKeyDataPojo anItem: item) {

                            String Key = anItem.getKeyword();
                            String Des = anItem.getDescription();

                            SearchKeyDataPojo searchKeyDataPojo = new SearchKeyDataPojo();

                            searchKeyDataPojo.setKeyword(Key);
                            searchKeyDataPojo.setDescription(Des);

                            mList.add(searchKeyDataPojo);
                            DictionaryActivityAdapter serviceCostAdapter = new DictionaryActivityAdapter(DictionaryActivity.this, mList, new DictionaryActivityAdapter.DictSelectedData() {
                                @Override
                                public void DictData(String Keyword, String Description) {

                                    recyclerView.setVisibility(View.GONE);
                                    frame.setVisibility(View.VISIBLE);
//                                    Toast.makeText(DictionaryActivity.this, "Please wait..", Toast.LENGTH_SHORT).show();

                                    Bundle bundle = new Bundle();
                                    bundle.putString("Keyword", Keyword);
                                    bundle.putString("Description", Description);
                                    DictionaryDetailFragment fragment1 = new DictionaryDetailFragment();
                                    fragment1.setArguments(bundle);

                                    FragmentManager fragmentManager = getSupportFragmentManager();
                                    fragmentManager.beginTransaction()
                                            .replace(R.id.frame,fragment1)
                                            .addToBackStack(null)
                                            .commit();
                                    fragmentManager.executePendingTransactions();
                                }
                            });
                            recyclerView.setAdapter(serviceCostAdapter);
                        }



                    } else {

                        Toast.makeText(DictionaryActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(DictionaryActivity.this, "Unable to connect to server", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchKeyPojo> call, Throwable t) {
                progressBar.dismiss();
                try {
                    Toast.makeText(DictionaryActivity.this, "Server Problem, Please try again later", Toast.LENGTH_SHORT).show();

                } catch (NullPointerException e) {
                }
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 1){

            Intent i = new Intent(DictionaryActivity.this, DictionaryActivity.class);
            startActivity(i);
        }else if (count == 0){
            Intent i = new Intent(DictionaryActivity.this, NavigationActivity.class);
            startActivity(i);
        }

        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            int count = getSupportFragmentManager().getBackStackEntryCount();

            if (count == 1){

                Intent i = new Intent(DictionaryActivity.this, DictionaryActivity.class);
                startActivity(i);
            }else if (count == 0){
                Intent i = new Intent(DictionaryActivity.this, NavigationActivity.class);
                startActivity(i);
            }
        }
        return false;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
