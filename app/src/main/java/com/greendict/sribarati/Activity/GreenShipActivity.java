package com.greendict.sribarati.Activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.greendict.sribarati.R;

public class GreenShipActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_green_ship);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("GreenShip");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        text = (TextView) findViewById(R.id.text);
        text.setText("GreenSHIP is the ASIA first company to develop ship design and operation software " +
                "for marine industry. Dr. Elangovan Muniyandy along with a group of technical experts " +
                "from different domain (Naval architect, CFD, Modelling, Database, ship operations " +
                "and software) came together with a single minded devotion to work collectively to " +
                "indigenously develop Ship Design and operation technology. GreenSHIP objective is " +
                "to develop technology that takes care of all aspects of ship design and operations." +
                " The company is also sensitive to the existing environmental concerns and wishes " +
                "to do its bit by implementing designs and safe practices that are eco-friendly. " +
                "GreenSHIP is also open to associating with interested and like-minded establishments," +
                " enterprises or governmental bodies in its research and development endeavor to " +
                "design and develop some of the best technologies and solutions that are on par " +
                "with the globally best. Please visit www.greenship.in");

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
